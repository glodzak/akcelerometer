
/**
 * @file    Cvicenie_6.cpp
 * @brief   Application entry point.
 */

#include "Akcelerometer.h"

pit_config_t pitConfig;

float pi = 3.14159265;
float fc = 1;
float T = 0.1;

extern "C" void PIT_LED_HANDLER(void)
{
	//Clear interrupt flag
	PIT_ClearStatusFlags(PIT, kPIT_Chnl_0,kPIT_TimerFlag);
	pitIsrFlag = true;

}

void INIT_PIT(void){

    PIT_GetDefaultConfig(&pitConfig);
    PIT_Init(PIT, &pitConfig);
    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(100000, PIT_SOURCE_CLOCK));
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);
    EnableIRQ(PIT_IRQ_ID);

    PIT_StartTimer(PIT, kPIT_Chnl_0);
}

void INIT_BOARD(void){
	/* Init board hardware. */
		BOARD_InitBootPins();
		BOARD_InitBootClocks();
		BOARD_InitBootPeripherals();

		/* Init FSL debug console. */
		BOARD_InitDebugConsole();

		LED_BLUE_INIT(kGPIO_DigitalOutput);
		LED_RED_INIT(kGPIO_DigitalOutput);

}

float dolno_priepustny_filter(float os_DPF,float* aktualna_os)
{

	float alfa = (2*pi*T*fc)/(2*pi*T*fc + 1);

	os_DPF = os_DPF + (alfa*((*aktualna_os) - os_DPF));

	return os_DPF;
}
float horno_priepustny_filter(float os_HPF,float* pred_os,float* aktualna_os)
{
	float alfa = 1/(2*pi*T*fc + 1);

	os_HPF = alfa*os_HPF + (alfa*(*aktualna_os - *pred_os));
	*pred_os = *aktualna_os;

	return os_HPF;
}


