/*
 * main.cpp
 *
 *  Created on: 27. 11. 2018
 *      Author: tomik
 */

#include "Akcelerometer.h"

volatile bool pitIsrFlag = false;
MMA8451Q* akcelerometer;

extern "C" void PORTA_IRQHandler(void){
	GPIO_ClearPinsInterruptFlags(GPIOA, (1<<15));

	PRINTF("Interrupt!\n\r");

	akcelerometer->FreeFall_Detect();
}


int main(void){

	INIT_BOARD();		//init board
	INIT_PIT();

	NVIC_EnableIRQ(PORTA_IRQn);
	MMA8451Q acc(0x1D);
	akcelerometer= &acc;


	float pred_os[3];
	float hpf_os[3]={0,0,0};
	float dpf_os[3]={0,0,0};
	float akt_os[3]={0,0,0};

	while(1) {

		SMC_PreEnterStopModes();

	    	if(true == pitIsrFlag)
	    	{
	    		LED_BLUE_TOGGLE();
				akt_os[0] = acc.getAccX();
				hpf_os[0] = horno_priepustny_filter(hpf_os[0], pred_os, akt_os);
				dpf_os[0] = dolno_priepustny_filter(dpf_os[0], akt_os);


				akt_os[1] = acc.getAccY();
				hpf_os[1] = horno_priepustny_filter(hpf_os[1], pred_os+1, akt_os+1);
				dpf_os[1] = dolno_priepustny_filter(dpf_os[1], akt_os+1);


				akt_os[2] = acc.getAccZ();
				hpf_os[2] = horno_priepustny_filter(hpf_os[2], pred_os+2, akt_os+2);
				dpf_os[2] = dolno_priepustny_filter(dpf_os[2], akt_os+2);

				PRINTF("\nHP filter:\n");
				PRINTF("x=%d",(int)(hpf_os[0]*100));
				PRINTF(", y=%d",(int)(hpf_os[1]*100));
				PRINTF(", z=%d\r",(int)(hpf_os[2]*100));

				PRINTF("\n\nDP filter:");
				PRINTF("\nx=%d",(int)(dpf_os[0]*100));
				PRINTF(", y=%d",(int)(dpf_os[1]*100));
				PRINTF(", z=%d\r\n",(int)(dpf_os[2]*100));



				for( uint32_t a = 0x250000; a>0 ;a-- ) {
				}
				pitIsrFlag = false;
	    	}

	    	// Uspatie procesora
	    	PRINTF("Vykonavam cyklus\n\r");
	    	SMC_SetPowerModeStop(SMC, kSMC_PartialStop);  // nezobudi ho nič
	    	//SMC_SetPowerModeVlpw(SMC);					// zobudi ho GPIO a PIT
	    	//SMC_SetPowerModeLls(SMC);					// zobudi ho GPIO
	    	SMC_PostExitStopModes();



	    }
	    return 0;
	}
